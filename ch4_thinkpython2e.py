#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  4 11:37:05 2022

@author: jmcelroy
"""

import turtle, random, time, math

bob = turtle.Turtle()
#bob.fd(100)
#time.sleep(3)

t = bob
n = 7
length = 70

def square(t, length):
    for i in range(4):
        t.fd(length)
        t.lt(90)
        
def circle(t, r):
    #circumference = 2*math.pi*r
    #n = int(circumference/3) + 3
    #length = circumference/n
    #polygon(t, n, length)
    arc(t, r, 360)

def polygon(t, n, length):
    if not isinstance(n, int):
        print('n must be an integer.')
    else:
        angle = 360/n
        polyline(t, n, length, angle)
        
def polyline(t, n, length, angle):
    """
    Draws n line segments with given length and angle in degrees between them. 
    
    Parameters
    ----------
    t : Turtle
    n : Number of line segments

    Returns
    -------
    None.
    
    """
    for i in range(n):
        t.fd(length)
        t.lt(angle)
        
def arc(t, r, angle):
    arc_length = 2*math.pi*r * angle/360
    n = int(arc_length/3) + 1
    step_length = arc_length/n
    step_angle = float(angle)/n
    polyline(t, n, step_length, step_angle)

circle(t, 40)
#square(t, 100)
#polygon(t, n, length)
time.sleep(2)
turtle.bye()
