#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  8 12:43:51 2022
@author: jmcelroy
Variations on https://www.pythonguis.com/tutorials/pyqt6-actions-toolbars-menus/
"""

from PyQt6 import *
from PyQt6.QtWidgets import *
from PyQt6.QtCore import *
from PyQt6.QtGui import *

import sys, random


class MainWindow(QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()

        self.setWindowTitle('App 01')

        label = QLabel('Greetings Professor Falken')
        label.setAlignment(Qt.AlignmentFlag.AlignCenter)

        self.setCentralWidget(label)

        toolbar = QToolBar('Bar of tools')
        toolbar.setIconSize(QSize(20, 20))
        self.addToolBar(toolbar)
        
        button_action = QAction(QIcon('android.png'), '1st button action', self)
        button_action.setStatusTip('button action info')
        button_action.triggered.connect(self.onMyToolBarButtonClick)
        button_action.setCheckable(True)
        button_action.setShortcut(QKeySequence('Ctrl+p'))
        
        toolbar.addSeparator()
        
        button_action_2 = QAction(QIcon('arrow-circle.png'), '2nd button action', self)
        button_action_2.setStatusTip('2nd button info')
        button_action_2.triggered.connect(self.onMyToolBarButtonClick)
        button_action_2.setCheckable(True)
        
        toolbar.addWidget(QLabel('Toolbar'))
        toolbar.addAction(button_action)
        toolbar.addAction(button_action_2)
        toolbar.addWidget(QCheckBox())
        
        menu = self.menuBar()
        file_menu = menu.addMenu('&File')
        file_menu.addAction(button_action)
        file_menu.addAction(button_action_2)
        file_submenu = file_menu.addMenu('same')
        file_submenu.addAction(button_action)
        file_submenu.addAction(button_action_2)
        
        button = QPushButton('press for dialogue')
        button.clicked.connect(self.button_clicked)
        self.setCentralWidget(button)
        

    def onMyToolBarButtonClick(self, s):
        print('click', s)
        
    def button_clicked(self, s):
        dlg = QMessageBox(self)
        dlg.setWindowTitle('I have a question')
        dlg.setText('Sample dialogue')
        dlg.setStandardButtons(QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.No)
        dlg.setIcon(QMessageBox.Icon.Question)
        button = dlg.exec()
        
        # pyqt6 syntax for standardbutton
        #https://stackoverflow.com/questions/65735260/
        if button == QMessageBox.StandardButton.Ok:
            print('ok')
        elif button == QMessageBox.StandardButton.Yes:
            print('yes')
        elif button == QMessageBox.StandardButton.No:
            print('no')

app = QApplication(sys.argv)
w = MainWindow()
w.show()
app.exec()