#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 10 07:04:59 2022
@author: jmcelroy
Misc notes from the book by Jamie Chan
"""

import sys, os

username = os.getlogin()
py_modules_path = '/home/' + username + '/python_modules'

if py_modules_path not in sys.path:
    sys.path.append(py_modules_path)
    
print('/home/' + username + '/python_modules')

message = 'price of {0:s} laptop is {1:d} USD. \
Exchange rate is {2:4.2f} USD to 1 EUR'.format('Apple', 1299, 1.23523)
        
print(message)

message = 'price of {} laptop is {} USD. \
Exchange rate is {} USD to 1 EUR'.format('Apple', 1299, 1.23523)
        
print(message)

# items[1:10:2] - every 2nd item

print('''this message
spans over
three lines''')

myInt = 11
print('Task A' if myInt == 10 else 'Task B')

print('Hello world'.replace('world', 'universe'))
print('Hello world'.count('o', 2, 9))