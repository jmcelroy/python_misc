#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 16 17:28:30 2022
@author: jmcelroy
Compiled while exploring NLTK stuff, will be added to over time. 
"""

from __future__ import division
from nltk import FreqDist

def lexical_diversity(text):
    return print('Each word is used an average of %.2f times.' % (len(text)/len(set(text))) )

def word_percentage(text, word):
    return print('The word "%s" takes up %.2f%% of %s.' % (word, (100*text.count(word)/len(text)), text) )

def size_vocabulary(text):
    return print('There are %d words in %s.' % (len(set(text)), text) )

def cumulative_freq_plot(numwords, text):
    print('The top %d words in %s are:\n' % (numwords, text))
    
    fd = FreqDist(text)
    top_words = fd.most_common(numwords)
    print(fd.most_common(numwords))
    
    total = 0
    for i in range(0, numwords):
        #print(top_words[:numwords][i][1])
        total += top_words[:numwords][i][1]
    #print(total)
    
    print('\nAnd they take up a total of %.2f%% of the text, which has %d words.' % (100*total/len(text), len(text)) )
    return fd.plot(numwords, cumulative=True)

def words_by_length(length, text, print_list):
    word_set = [word for word in text if len(word) > length]
    
    if print_list == True:
        print(word_set)
        
    return print('There are %d words with length greater than %d.' % (len(word_set), length))

def words_by_length_and_freq(length, freq, text, print_list):
    
    fd = FreqDist(text)
    word_set = [word for word in text if len(word) > length and fd[word] > freq]
    
    if print_list == True:
        print(word_set)
        
    return print('There are %d words with length greater than %d which occur %d times.' % (len(word_set), length, freq))

def freq_length(length, text):
    word_length_list = [len(word) for word in text]
    fd = FreqDist(word_length_list)
    print('Most frequent word length is %d.' % fd.max())
    return print('Words of length %d are %.3f%% of the text.' % (length, fd.freq(length)*100)) 
    
    
    
    