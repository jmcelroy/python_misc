#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  8 15:24:20 2022
@author: jmcelroy
https://www.pythonguis.com/tutorials/pyqt6-creating-multiple-windows/
https://www.pythonguis.com/tutorials/pyqt6-system-tray-mac-menu-bar-applications/
"""

from PyQt6 import *
from PyQt6.QtWidgets import *
from PyQt6.QtCore import *
from PyQt6.QtGui import *

import sys, random

class WindowTwo(QWidget):
    
    # qwidget 'window'
    def __init__(self):
        super().__init__()
        layout = QVBoxLayout()
        self.label = QLabel('another window %d' %random.randint(0, 10))
        layout.addWidget(self.label)
        self.setLayout(layout)


class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()
        self.window1 = WindowTwo()
        self.window2 = WindowTwo()
        
        l = QVBoxLayout()
        button1 = QPushButton('window 1')
        button1.clicked.connect(
            lambda checked: self.toggle_window(self.window1)
        )
        l.addWidget(button1)

        button2 = QPushButton('window 2')
        button2.clicked.connect(
            lambda checked: self.toggle_window(self.window2)
        )
        l.addWidget(button2)

        w = QWidget()
        w.setLayout(l)
        self.setCentralWidget(w)
        
        #self.w = None
        #self.button = QPushButton('press to see 2nd window')
        #self.button.clicked.connect(self.show_new_window)
        #self.setCentralWidget(self.button)

    def show_new_window(self, checked):
        if self.w is None:
            self.w = WindowTwo()
            self.w.show()
        else:
            self.w.close()
            self.w = None
    
    def toggle_window(self, window):
        if window.isVisible():
            window.hide()
        else:
            window.show()
    
    def toggle_window1(self, checked):
        if self.window1.isVisible():
            self.window1.hide()
        else:
            self.window1.show()

    def toggle_window2(self, checked):
        if self.window2.isVisible():
            self.window2.hide()
        else:
            self.window2.show()


def copy_color_hex():
    if dialog.exec(): 
        """
        
        if dialog.exec_, program throws AttributeError: 
        'QColorDialog' object has no attribute 'exec_' 
        
        """
        color = dialog.currentColor()
        clipboard.setText(color.name())

def copy_color_rgb():
    if dialog.exec():
        color = dialog.currentColor()
        clipboard.setText('rgb(%d, %d, %d)' % (
            color.red(), color.green(), color.blue()
        ))

def copy_color_hsv():
    if dialog.exec():
        color = dialog.currentColor()
        clipboard.setText('hsv(%d, %d, %d)' % (
            color.hue(), color.saturation(), color.value()
        ))

app = QApplication(sys.argv)
app.setQuitOnLastWindowClosed(False)
icon = QIcon('android.png')

clipboard = QApplication.clipboard()
dialog = QColorDialog()

tray = QSystemTrayIcon()
tray.setIcon(icon)
tray.setVisible(True)

menu = QMenu()
#action = QAction('something')
#menu.addAction(action)

action1 = QAction('Hex')
action1.triggered.connect(copy_color_hex)
menu.addAction(action1)

action2 = QAction('RGB')
action2.triggered.connect(copy_color_rgb)
menu.addAction(action2)

action3 = QAction('HSV')
action3.triggered.connect(copy_color_hsv)
menu.addAction(action3)

quit = QAction('quit')
quit.triggered.connect(app.quit)
menu.addAction(quit)

tray.setContextMenu(menu)

w = MainWindow()
w.show()
app.exec()