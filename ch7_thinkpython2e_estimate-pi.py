#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  4 18:17:14 2022

@author: jmcelroy
"""

# pg 70
# Ramanujan's formula to approximate 1/pi

import math

def estimate_pi(input_num):
    
    pi_approx = 0
    k = 0
    
    for i in range(0, input_num):
        new_term = (2*math.sqrt(2)/9801)*((math.factorial(4*k)*(1103+26390*k))/(((math.factorial(k))**4)*(396**(4*k))))
        k += 1
        pi_approx = pi_approx + new_term
        
        if new_term < 1e-32:
            print('1/pi is approximately %s with k = %s.' % (pi_approx, input_num))
            break
        
        #print('newterm is approximately %s.' %new_term)
        #print('1/pi is approximately %s.' %pi_approx)

estimate_pi(5)

#print('1/pi is exactly %s.' %str(1/math.pi))

