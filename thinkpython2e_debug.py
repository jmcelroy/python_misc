#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul  9 07:43:41 2022

Functions from ends of chapters in Allen Downey's 
Think Python to help with debugging. 
"""

# Prints attributes' names and values
def print_attributes(obj):
    for attr in vars(obj):
        print(attr, getattr(obj, attr))

# Returns class that gave method's definition
def find_defining_class(obj, meth_name):
    for ty in type(obj).mro():
        if meth_name in ty.__dict__:
            return ty

# Prints str representation of objects
def print_string_rep(input_str):
    print(repr(input_str))
    
