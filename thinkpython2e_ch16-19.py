#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul  9 06:52:50 2022

@author: jmcelroy
"""

import math 

class Time:
    """
    """
    
    def __init__(self, hour=0, minute=0, second=0):
        self.hour = hour
        self.minute = minute
        self.second = second
    
    def __str__(self):
        return '%.2d:%.2d:%.2d' % (self.hour, self.minute, self.second)
    
    def print_time(time):
        print('%.2d:%.2d:%.2d' % (time.hour, time.minute, time.second))
        
    #def is_after(self, other):
    #    return self.time_to_int() > other.time_to_int()
    
start = Time()
start.hour = 9
start.minute = 45
start.second = 30

Time.print_time(start)

start.print_time()

end = Time()
end.hour = 12
end.minute = 45
end.second = 30

#end.is_after(start)

time = Time()
Time.print_time(time)

time = Time(9, 45, 31)
print(time)

x = 0
y = math.log(x) if x > 0 else float('nan')
print(y)

print(any(letter == 't' for letter in 'monty'))

from collections import defaultdict
   
# Defining the dict
d = defaultdict(int)
   
numbers = [4, 4, 3, 3, 2, 3, 4, 2, 4, 1, 2]
   
for i in numbers:
    d[i] += 1  # gives # of times each number occurs
print(d)

