#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul  9 06:14:33 2022
@author: jmcelroy
"""

import copy

class Point:
    """
    Represents 2D point
    """

blank = Point()
blank.x = 3.0
blank.y = 4.0

def print_point(p):
    try:
        if isinstance(p, Point) == True:
            print('(%g, %g)' % (p.x, p.y))
    except AttributeError:
        print('wrong object type')
    
print_point(blank)

class Rectangle:
    """
    Represents rectangle
    """

box = Rectangle()
box.width = 100
box.height = 200
box.corner = Point()
box.corner.x = 0
box.corner.y = 0

def find_center(rect):
    p = Point()
    p.x = rect.corner.x + rect.width/2
    p.y = rect.corner.y + rect.height/2
    return p

center = find_center(box)
print_point(center)

p2 = copy.deepcopy(blank)
print_point(p2)
print(p2 is blank)

