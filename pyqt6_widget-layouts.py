#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  8 11:57:58 2022
@author: jmcelroy
Variations on https://www.pythonguis.com/pyqt6-tutorial/
"""

from PyQt6 import *
from PyQt6.QtWidgets import *
from PyQt6.QtCore import *
from PyQt6.QtGui import *

import sys, random

#from layout_colorwidget import Color

class Color(QWidget):

    def __init__(self, color):
        super(Color, self).__init__()
        self.setAutoFillBackground(True)

        palette = self.palette()
        palette.setColor(QPalette.ColorRole.Window, QColor(color))
        self.setPalette(palette)

class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()

        self.setWindowTitle("My App")
        
        tabs = QTabWidget()
        tabs.setTabPosition(QTabWidget.TabPosition.West)
        # new pyqt6 syntax
        # https://ru.stackoverflow.com/questions/1410167/
        tabs.setMovable(True)
        
        for n, color in enumerate(['red', 'green', 'blue', 'orange']):
            tabs.addTab(Color(color), color)
            
        self.setCentralWidget(tabs)
        
        """pagelayout = QVBoxLayout()
        button_layout = QHBoxLayout()
        self.stacklayout = QStackedLayout()
        
        pagelayout.addLayout(button_layout)
        pagelayout.addLayout(self.stacklayout)
        
        btn = QPushButton('red')
        btn.pressed.connect(self.activate_tab_1)
        button_layout.addWidget(btn)
        self.stacklayout.addWidget(Color('red'))
        
        btn = QPushButton('yellow')
        btn.pressed.connect(self.activate_tab_2)
        button_layout.addWidget(btn)
        self.stacklayout.addWidget(Color('yellow'))
        
        btn = QPushButton('orange')
        btn.pressed.connect(self.activate_tab_3)
        button_layout.addWidget(btn)
        self.stacklayout.addWidget(Color('orange'))
        
        pagelayout.setContentsMargins(20,20,20,20)
        pagelayout.setSpacing(20)
        
        sublayout_one = QVBoxLayout()
        sublayout_two = QVBoxLayout()
        
        layout.addWidget(Color('green'))
        layout.addWidget(Color('red'))
        layout.addWidget(Color('blue'))
        
        layout.addLayout(sublayout_one)
        
        layout.addWidget(Color('green'))
        layout.addWidget(Color('red'))
        layout.addWidget(Color('blue'))
        
        layout.addLayout(sublayout_two)
        
        widget = QWidget()
        widget.setLayout(pagelayout)
        self.setCentralWidget(widget)"""
        
    def activate_tab_1(self):
        self.stacklayout.setCurrentIndex(0)

    def activate_tab_2(self):
        self.stacklayout.setCurrentIndex(1)

    def activate_tab_3(self):
        self.stacklayout.setCurrentIndex(2)

app = QApplication(sys.argv)

window = MainWindow()
window.show()

app.exec()