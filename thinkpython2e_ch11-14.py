#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  5 16:20:25 2022

@author: jmcelroy
"""

import dbm, pickle, os
been_called = False

def example():
    global been_called
    been_called = True
    return been_called
    
#print(example())

t = divmod(7, 3)
print(t)

def printall(*args):
    print(args)
    
#printall(1, 2, 3, 'idea')

"""def sum_all(*args):
    sum = 0
    for i in args:
        sum = sum + i
    print(sum)

sum_all(1, 2, 6)"""

# arbitrary # of input arguments 
def sum_all_2(*args):
    if len(args) == 1:
        return args[0]
    else:
        return args[-1] + sum_all_2(*args[:-1])

print(sum_all_2(0, 9, 9))

# optional input arguments
def multiply_num(num, num2 = 1):
    if num2 != 1:
        return num*num2
    else:
        return num
    
print(multiply_num(2))

"""db = dbm.open('test2', 'c')
db['test1'] = 'a test'
print(db['test1'])
t1 = [1, 2, 4]
s = pickle.dumps(t1)
t2 = pickle.loads(s)
print(t2)
db['test2'] = s
print(db['test2'])
db.close()"""

filename = 'README.md'
cmd = 'md5sum ' + filename
fp = os.popen(cmd)
res = fp.read()
stat = fp.close()
print(res)

s = '1 2\t 3\n 4'
print(repr(s))

