#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul  2 07:20:12 2022

@author: jmcelroy

Variations on https://www.pythonguis.com/pyqt6-tutorial/
"""

from PyQt6 import *
from PyQt6.QtWidgets import *
from PyQt6.QtCore import *
from PyQt6.QtGui import *

import sys
from random import choice

window_titles = [
    'App',
    'App 2',
    'Window title',
    'Words here',
    'Wrong'
    ]

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        
        self.setWindowTitle("Widgets App")

        layout = QVBoxLayout()
        widgets = [
            QCheckBox,
            QComboBox,
            QDateEdit,
            QDateTimeEdit,
            QDial,
            QDoubleSpinBox,
            QFontComboBox,
            QLCDNumber,
            QLabel,
            QLineEdit,
            QProgressBar,
            QPushButton,
            QRadioButton,
            QSlider,
            QSpinBox,
            QTimeEdit,
        ]

        for w in widgets:
            layout.addWidget(w())

        widget = QWidget()
        widget.setLayout(layout)
        self.setCentralWidget(widget)
        
        
        #self.n_times_clicked = 0
"""        self.setWindowTitle('An App')
        
        self.label = QLabel()
        self.input = QLineEdit()
        self.input.textChanged.connect(self.label.setText)
        
        layout = QVBoxLayout()
        layout.addWidget(self.input)
        layout.addWidget(self.label)
        layout.addWidget(self.label)
        
        container = QWidget()
        container.setLayout(layout)
        
        self.setCentralWidget(container)
        
        #self.setCentralWidget(self.label)
        
        self.button = QPushButton('Click here')
        self.button.clicked.connect(self.button_clicked)
        
        self.windowTitleChanged.connect(self.window_title_changed)
        
        self.setCentralWidget(self.button)
        
        
        
        
        
        
        self.button.setCheckable(True)
        button.clicked.connect(self.button_toggled)
        
        self.button.released.connect(self.button_released)
        self.button.setChecked(self.button_is_checked)
        
        self.setFixedSize(QSize(400, 200))
        button.setAlignment(Qt.AlignCenter)
    
    def contextMenuEvent(self, e):
        context = QMenu(self)
        context.addAction(QAction('test 1', self))
        context.addAction(QAction('test 2', self))
        context.addAction(QAction('test 3', self))
        context.exec(e.globalPos())
    
    def mouseMoveEvent(self, e):
        self.label.setText("mouseMoveEvent")

class CustomButton(QPushButton):
    def mousePressEvent(self, e):
        
        print('Mouse pressed')
        super(self, MainWindow).contextMenuEvent(e)
        e.accept()
        
        if e.button() == Qt.MouseButton.LeftButton:
            # handle the left-button press in here
            self.label.setText("mousePressEvent LEFT")

        elif e.button() == Qt.MouseButton.MiddleButton:
            # handle the middle-button press in here.
            self.label.setText("mousePressEvent MIDDLE")

        elif e.button() == Qt.MouseButton.RightButton:
            # handle the right-button press in here.
            self.label.setText("mousePressEvent RIGHT")

    def mouseReleaseEvent(self, e):
        if e.button() == Qt.MouseButton.LeftButton:
            self.label.setText("mouseReleaseEvent LEFT")

        elif e.button() == Qt.MouseButton.MiddleButton:
            self.label.setText("mouseReleaseEvent MIDDLE")

        elif e.button() == Qt.MouseButton.RightButton:
            self.label.setText("mouseReleaseEvent RIGHT")

    def mouseDoubleClickEvent(self, e):
        if e.button() == Qt.MouseButton.LeftButton:
            self.label.setText("mouseDoubleClickEvent LEFT")

        elif e.button() == Qt.MouseButton.MiddleButton:
            self.label.setText("mouseDoubleClickEvent MIDDLE")

        elif e.button() == Qt.MouseButton.RightButton:
            self.label.setText("mouseDoubleClickEvent RIGHT") 
    
    def window_title_changed(self, window_title):
        print('Window title changed to: %s' % window_title)
        if window_title == 'Wrong':
            self.button.setDisabled(True)
        
    def button_clicked(self):
        #self.button.setText('You already clicked me.')
        #self.button.setEnabled(False)
        print('Clicked')
        new_window_title = choice(window_titles)
        print('Setting title: %s' %new_window_title)
        self.setWindowTitle(new_window_title)
        
        
    def button_toggled(self, checked):
        self.button_is_checked= checked
        print('Checked?', checked)
    def button_released(self):
        self.button_is_checked = self.button_isChecked()
        print(self.button_is_checked)
        
class MainWindowTwo(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindowTwo, self).__init__(*args, **kwargs)
        
        self.setWindowTitle('An App 2')
        
        label = QLabel('This is fun')
        label.setAlignment(Qt.AlignCenter)
        
        self.setFixedSize(QSize(400, 200))
        self.setCentralWidget(label)"""

app = QApplication(sys.argv)
#window = QWidget()
window = MainWindow()
window.show() # otherwise hidden by default

#window2 = MainWindowTwo()
#window2.show()

app.exec()